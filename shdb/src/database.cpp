#include "database.h"

#include <memory>

#include "fixed.h"
#include "flexible.h"

namespace shdb
{

Database::Database(const std::filesystem::path & path, FrameIndex frame_count)
    : statistics(std::make_shared<Statistics>())
    , store(std::make_shared<Store>(path, frame_count, statistics))
    , catalog(std::make_shared<Catalog>(store))
{
}

void Database::createTable(const std::filesystem::path & name, std::shared_ptr<Schema> schema)
{
    catalog->saveTableSchema(name, schema);
    store->createTable(name);
}

std::shared_ptr<ITable> Database::getTable(const std::filesystem::path & name, std::shared_ptr<Schema> schema)
{
    if (!schema)
        schema = catalog->findTableSchema(name);

    auto provider = createPageProvider(std::move(schema));
    return store->openTable(name, std::move(provider));
}

bool Database::checkTableExists(const std::filesystem::path & name)
{
    return store->checkTableExists(name);
}

void Database::dropTable(const std::filesystem::path & name)
{
    catalog->forgetTableSchema(name);
    store->removeTable(name);
}

std::shared_ptr<Statistics> Database::getStatistics()
{
    return statistics;
}

std::shared_ptr<Schema> Database::findTableSchema(const std::filesystem::path & name)
{
    return catalog->findTableSchema(name);
}

std::shared_ptr<IPageProvider> Database::createPageProvider(std::shared_ptr<Schema> schema)
{
    for (const auto & column : *schema)
        if (column.type == Type::string)
            return createFlexiblePageProvider(std::move(schema));

    return createFixedPageProvider(std::move(schema));
}

std::shared_ptr<Database> connect(const std::filesystem::path & path, FrameIndex frame_count)
{
    if (!std::filesystem::exists(path))
        std::filesystem::create_directories(path);

    return std::make_shared<Database>(path, frame_count);
}

}
