#pragma once

#include <functional>

namespace shdb
{

constexpr int page_size = 4096;

using PageIndex = int;
using PageId = std::pair<int, PageIndex>;

}

namespace std
{

template <>
class hash<shdb::PageId>
{
public:
    size_t operator()(const shdb::PageId & page_id) const { return std::hash<int>()(page_id.first) ^ std::hash<int>()(page_id.second); }
};

}
