#pragma once

#include <filesystem>

#include "catalog.h"
#include "schema.h"
#include "statistics.h"
#include "store.h"
#include "table.h"

namespace shdb
{

class Database
{
public:
    Database(const std::filesystem::path & path, FrameIndex frame_count);

    void createTable(const std::filesystem::path & name, std::shared_ptr<Schema> schema);

    std::shared_ptr<ITable> getTable(const std::filesystem::path & name, std::shared_ptr<Schema> schema = nullptr);

    bool checkTableExists(const std::filesystem::path & name);

    void dropTable(const std::filesystem::path & name);

    std::shared_ptr<Statistics> getStatistics();

    std::shared_ptr<Schema> findTableSchema(const std::filesystem::path & name);

private:
    std::shared_ptr<IPageProvider> createPageProvider(std::shared_ptr<Schema> schema);

    std::shared_ptr<Statistics> statistics;
    std::shared_ptr<Store> store;
    std::shared_ptr<Catalog> catalog;
};

std::shared_ptr<Database> connect(const std::filesystem::path & path, FrameIndex frame_count);

}
