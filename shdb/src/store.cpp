#include "store.h"

namespace shdb
{

Store::Store(const std::filesystem::path & path, FrameIndex frame_count, std::shared_ptr<Statistics> statistics)
    : buffer_pool(std::make_shared<BufferPool>(std::move(statistics), frame_count)), path(path)
{
}

void Store::createTable(const std::filesystem::path & name)
{
    auto file = std::make_unique<File>(path / name, true);
}

std::shared_ptr<ITable> Store::openTable(const std::filesystem::path & name, std::shared_ptr<IPageProvider> provider)
{
    auto file = std::make_unique<File>(path / name, false);
    return shdb::createTable(buffer_pool, std::move(file), std::move(provider));
}

bool Store::checkTableExists(const std::filesystem::path & name)
{
    return std::filesystem::exists(path / name);
}

void Store::removeTable(const std::filesystem::path & name)
{
    std::filesystem::remove(path / name);
}

}
