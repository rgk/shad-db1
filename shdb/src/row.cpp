#include "row.h"

#include <sstream>

namespace shdb
{

template <class... Ts>
struct Overloaded : Ts...
{
    using Ts::operator()...;
};

template <class... Ts>
Overloaded(Ts...) -> Overloaded<Ts...>;

std::string toString(const Row & row)
{
    std::stringstream stream;
    stream << '[';
    for (size_t index = 0; index < row.size(); ++index)
    {
        const auto & value = row[index];
        std::visit(
            Overloaded{
                [&](const Null &) { stream << "NULL"; },
                [&](const uint64_t & value) { stream << value; },
                [&](const bool & value) { stream << (value ? "true" : "false"); },
                [&](const std::string & value) { stream << value; },
            },
            value);
        if (index < row.size() - 1)
            stream << ',';
    }
    stream << ']';
    return stream.str();
}

bool Null::operator==(const Null &) const
{
    return true;
}

bool Null::operator!=(const Null &) const
{
    return false;
}

bool RowId::operator==(const RowId & other) const
{
    return page_index == other.page_index && row_index == other.row_index;
}

bool RowId::operator!=(const RowId & other) const
{
    return !(*this == other);
}

}
