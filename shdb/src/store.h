#pragma once

#include <filesystem>

#include "bufferpool.h"
#include "schema.h"
#include "statistics.h"
#include "table.h"

namespace shdb
{

class Store
{
public:
    Store(const std::filesystem::path & path, FrameIndex frame_count, std::shared_ptr<Statistics> statistics);

    void createTable(const std::filesystem::path & name);

    std::shared_ptr<ITable> openTable(const std::filesystem::path & name, std::shared_ptr<IPageProvider> provider);

    void removeTable(const std::filesystem::path & name);

    bool checkTableExists(const std::filesystem::path & name);

private:
    std::shared_ptr<BufferPool> buffer_pool;
    std::filesystem::path path;
};

}
